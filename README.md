## 1.Общие запросы  
### 1.1 Получение компаний клиента, отображаемых в сайдбаре, загружаются при первой отрисовке сайдбара.  
   
   request: GET  /companies  

[response](./stubbs/Home/companies.json)

```text
[
  {
    "id": "1", // идентификатор компании
    "shortName": "ИП Колчева Юлия Дмитриевна" // короткое имя компании
    "accounts": [
      {
        "id": <String> "453211211545ew12121sfdf5", // идентификатор счёта
        "getInMonth":<Number> 250000, // пришло за текущий месяц, согласно макету
        "sendInMonth":<Number> 113567, // ушло за текущий месяц, согласно макету
        "val":<String> "RUB", // валюта для счёта,  
        "number":<String> "40802810310050033666", // номер счёта
        "value":<Number> 1, // уникальное значение которое используется для select, чтобы
            идентифицировать соответствующий счет ( в select мы же видим текст, а не номера)
            
        "name":<String> "Карточный счёт *0978", // тип счёта
        "sum":<String|Number> "10 080,56 ₽", // сумма на счёте, должно быть number
            так как производим операции над ним. 
            
        "title": "ИП Колчева Юлия Дмитриевна" // владелец счета(компания к которой относится данный счёт), 
            но из макетов не понятно как тогда в списках со счетами отображаются чужие счета, например для страницы платежей между счетами
      }, 
    ]
  },
]
```

accounts описанные выше используются потом на всех остальных страницах

## 2. Главная 

Для этой страницы используются запросы: 

### 2.1. Получение платежей для отображения в списке внизу главной страницы.

request: GET /bills?query

query может содержать следующие значения:
`tab=<Number>
day=<Date>
start=<Date>
end=<Date>`
где tab показывает тип платежей - все, входящие, исходящие или на подпись.

day фильтр по определенной дате.

start и end всегда указываются вместе, показывают период за который должны отобразиться платежи.

response: в зависимости от типа tab 

tab = 0 [response](./stubbs/Home/listBillsAll.json)

tab = 1 response пустой массив, для текущей версии.

tab = 2 [response](./stubbs/Home/listBillsOutbox.json)

tab = 3 [response](./stubbs/Home/listBillsAPaid.json)

```text
   [
      {
        "id": <String|Number> "2432321325gda3242343242234232", //уникальный идентификатор для элемента массива результатов
        "date": <Date> "2018-11-01T13:51:50.417Z", // дата по которой сгруппированы платежи
        "list": <ArrayOf(<Object>)> [ 
          {
            "id": <String|Number> "989eet2tr3242343242234232", // идентификатор платежа
            "title": <String> "Зарплатный проект и повторяющиеся платежи",
            "description": <String> "Новые функции: Чтобы вам было удобно платить регулярные платежи, ...",
            "type": "news"
          },
          {
              "idPayment": "987eet2tr3242343242234232",
              "sum": <Number> 8567.56, // сколько было заплачено/получено
              "type": <String> "outbox", // входящий или исходящий платеж
              "status": <String> "process", //статус платежа
              "income": <Boolean> false // входящий или исходящий платеж
              "recipient": <Object> { // кому был отправлен платеж
                title: <String> 'ИП Стурза Наталья Александровна'
                number: <String> '0495146088'
                code: <String> '301018102'
              },
              "bank": <Object> { // информация о банке
                title: <String> 'АБАКАНСКОЕ ОТДЕЛЕНИЕ №8602 ПАО СБЕРБАНК' // название
                bic: <String> '049514608' // БИК
                correspondentAcc: <String> '30101810500000000608' // корресподентский счёт
              },
              "purpose": <String> // назначение платежа
           },
        ]
    }
```
 
На наш взгляд, запрос нужно перенести на этот URL:

GET /companies/:id/bills?query

На макете используется запись новости поэтому type news включен в ответ.

### 2.2. Запрос на отправку определенного платежа

request POST /payments/send

payload:  здесь получается что на бэке уже сохранены эти платежи и мы можем отправить
только id платежей, которые мы хотим провести, либо цельный объект как ниже.
```text
    {
      idPayment: <String>,
      numberPayment: <Number>, // номер платежа
      idUser: user.id,
      status: enum('sending', 'process', 'noSignature'),
      createdAt: <Date>,
      type: enum('legalEntity', 'physicalEntity', 'betweenAccounts', 'toBudget', 'fromFile', 'inCustom'),
      purpose: <String>,
      sum: <Number>,
      accountNumber: <String>, // номер счета
      account: <Number>, value для select из списка счетов
      taxes: enum('Без НДС', 'НДС 10%', 'НДС 20%'),
      urgently: <Boolean>, // отправить срочно
      priority: <Number>, // очередность
      recipient: { // получатель
          title: <String>, // наименование организации/ФИО
          number: <String>, // ИНН
          code: <String>, // КПП
      },
      recipientName: <String>, // для платежа между счетами
      senderName: <String>, // для платежа между счетами
      email: <String>,
      bank: <Object> {
          title: <String>,
          bic: <String>,
          correspondentAcc: <String>,
      },
      // в бюджет/ в таможню
      kbk: <String>,
      uin: <String>,
      payer: { // плательщик
          title: <String>,
          number: <String>, // ИНН
      },
      classifier: <String>, // ОКТМО
      basisPayment: <String>, // Основание платежа
      taxablePeriod: <String>, // Налоговый период
      numberDocument: <String>, // В требовании от налоговой
      dateDocument: <String>, // пример даты хранимой 
        на сервере для даты документа указанной в платежке "2018-11-01T13:51:50.417Z"
      
      codeStatus: <String>,// Код статуса плательщика
      codeCustom: <String>, // Код таможенного органа
    }
```
        
response: `{ anyField: true } ` или статус 200 о том что все ок, введенный пароль соответствует паролю на сервере

### 2.3. Запрос на отправку платежей

request POST /payments/send

payload:  здесь получается что на бэке уже сохранены эти платежи и мы можем отправить
только id платежей, которые мы хотим провести, либо цельный объект как ниже.
```text
    {
      idPayment: <String>,
      numberPayment: <Number>, // номер платежа
      idUser: user.id,
      status: enum('sending', 'process', 'noSignature'),
      createdAt: <Date>,
      type: enum('legalEntity', 'physicalEntity', 'betweenAccounts', 'toBudget', 'fromFile', 'inCustom'),
      purpose: <String>,
      sum: <Number>,
      accountNumber: <String>, // номер счета
      account: <Number>, value для select из списка счетов
      taxes: enum('Без НДС', 'НДС 10%', 'НДС 20%'),
      urgently: <Boolean>, // отправить срочно
      priority: <Number>, // очередность
      recipient: { // получатель
          title: <String>, // наименование организации/ФИО
          number: <String>, // ИНН
          code: <String>, // КПП
      },
      recipientName: <String>, // для платежа между счетами
      senderName: <String>, // для платежа между счетами
      email: <String>,
      bank: <Object> {
          title: <String>,
          bic: <String>,
          correspondentAcc: <String>,
      },
      // в бюджет/ в таможню
      kbk: <String>,
      uin: <String>,
      payer: { // плательщик
          title: <String>,
          number: <String>, // ИНН
      },
      classifier: <String>, // ОКТМО
      basisPayment: <String>, // Основание платежа
      taxablePeriod: <String>, // Налоговый период
      numberDocument: <String>, // В требовании от налоговой
      dateDocument: <String>, // пример даты хранимой 
         на сервере для даты документа указанной в платежке "2018-11-01T13:51:50.417Z"
      codeStatus: <String>,// Код статуса плательщика
      codeCustom: <String>, // Код таможенного органа
    }
```
response: `{ anyField: true } ` или статус 200 о том что все ок, введенный пароль соответствует паролю на сервере

## 3. Платежи
Типы используемых платежей описаны ниже.
Для этой страницы используются запросы: 

### 3.1. Получение последних платежей для текущей компании.

request: GET /payments/:typeDocument

На наш взгляд, запрос нужно перенести на этот URL:

request: GET /companies/:id/payments/:typeDocument

response: массив последних платежей, соответствующих типу документа
[response](./stubbs/Payments/index.json)

### 3.2. Получение выбранного платежа из списка последних для текущей компании.

request: GET /payments/:typeDocument/:id

На наш взгляд, запрос нужно перенести на этот URL:

request: GET /companies/:id/payments/:typeDocument/:id

response: данные по платежу

### 3.3. Удаление платежа из списка последних для текущей компании.

request: DELETE /payments/:typeDocument/:id
  
### 3.4. Сохранение платежа без подписи.

request: POST /payments/save
 
**Тип "Юрлицу":**

```text
{
  idPayment: <String>, // идентификатор платежа (id)
  numberPayment: <Number>, // номер платежа в заголовке (Рублевый платёж № ...)
  idCompany: company.id, // идентификатор компании, из которой осуществляется платеж
  status: 'noSignature', // статус платежа (отправлен, в процессе, не подписан)
  createdAt: <Date>, // дата создания платежа
  type: 'legalEntity', // тип платежа - юрлицу
  purpose: <String>, // назначение
  sum: <Number>, // сумма на выбранном счете
  accountNumber: <String>, // номер счета
  account: <Number>, value для select из списка счетов - идентификатор (см. п. 1.2), по нему дополнительно определяется валюта, но ее лучше отдельным полем передавать 
  taxes: enum('Без НДС', 'НДС 10%', 'НДС 20%'), // налоги
  urgently: <Boolean>, // отправить срочно
  priority: <Number>, // очередность (в настройке платежа)
  recipient: { // получатель
      title: <String>, // наименование организации/ФИО
      number: <String>, // ИНН
      code: <String>, // КПП
  },
  email: <String>, почта, куда прислать платеж
  bank: <Object> {
      title: <String>, // название банка
      bic: <String>, // БИК
      correspondentAcc: <String>, // корреспондентский счет
  },
}
```

**Тип "Физлицу":**

```text
{
  idPayment: <String>, // идентификатор платежа (id)
  numberPayment: <Number>, // номер платежа в заголовке (Рублевый платёж № ...)
  idUser: user.id, // залогиненный в систему пользователь/ отправитель платежа
  status: 'noSignature', // статус платежа (отправлен, в процессе, не подписан)
  createdAt: <Date>, // дата создания платежа
  type: 'physicalEntity', // тип платежа - физлицу
  purpose: <String>, // назначение
  sum: <Number>, // сумма на выбранном счете
  accountNumber: <String>, // номер счета
  account: <Number>, value для select из списка счетов - идентификатор, по нему дополнительно определяется валюта, но ее лучше отдельным полем передавать
  urgently: <Boolean>, // отправить срочно
  priority: <Number>, // очередность
  recipient: { // получатель
      title: <String>, // наименование организации/ФИО
      number: <String>, // ИНН
  },
  email: <String>, почта, куда прислать платеж
  bank: <Object> {
      title: <String>, // название банка
      bic: <String>, // БИК
      correspondentAcc: <String>, // корреспондентский счет
  },
}
```

**Тип "Между счетами":**

```text
{
  idPayment: <String>, // идентификатор платежа (id)
  numberPayment: <Number>, // номер платежа в заголовке (Платёж между счетами № ...)
  idCompany: company.id, // идентификатор компании, из которой осуществляется платеж
  status: 'noSignature', // статус платежа (отправлен, в процессе, не подписан)
  createdAt: <Date>, // дата создания платежа
  type: 'betweenAccounts', // тип платежа - между счетами
  purpose: <String>, // назначение
  sum: <Number>, // сумма на выбранном счете
  accountNumber: <String>, // номер счета
  account: <Number>, // идентификатор счета, с которого осуществляется платеж
  accountTo: <Number>, // идентификатор счета, на который поступит платеж
  taxes: enum('Без НДС', 'НДС 10%', 'НДС 20%'), // налоги
  recipientName: <String>, // название компании/ФИО получателя - владельца счета
  senderName: <String>, // название компании/ФИО отправителя - владельца счета
}
```

**Тип "В бюджет":**

 ```text
{
  idPayment: <String>, // идентификатор платежа (id)
  numberPayment: <Number>, // номер платежа в заголовке (Платёж между счетами № ...)
  idCompany: company.id, // идентификатор компании, из которой осуществляется платеж
  status: 'noSignature', // статус платежа (отправлен, в процессе, не подписан)
  createdAt: <Date>, // дата создания платежа
  type: 'toBudget', // тип платежа - в бюджет
  purpose: <String>, // назначение
  sum: <Number>, // сумма на выбранном счете
  accountNumber: <String>, // номер счета
  account: <Number>, // идентификатор счета, с которого осуществляется платеж
  taxes: enum('Без НДС', 'НДС 10%', 'НДС 20%'), // налоги
  recipient: { // получатель
      title: <String>, // наименование организации/ФИО
      number: <String>, // ИНН
      code: <String>, // КПП
  },
  bank: <Object> {
          title: <String>, // название банка
          bic: <String>, // БИК
          correspondentAcc: <String>, // корреспондентский счет
      },
  kbk: <String>, // КБК
  uin: <String>, // уникальный идентификатор начисления
  payer: { // плательщик
      title: <String>, // наименование организации/ФИО
      number: <String>, // ИНН
  },
  classifier: <String>, // ОКТМО
  basisPayment: <String>, // Основание платежа
  taxablePeriod: <String>, // Налоговый период
  numberDocument: <String>, // В требовании от налоговой
  dateDocument: <String>, // Дата документа (поле для выбора даты в календаре)
  codeStatus: <String>,// Код статуса плательщика
  codeCustom: <String>, // Код таможенного органа
}
```

**Тип "В таможню":**

```text
{
  idPayment: <String>, // идентификатор платежа (id)
  numberPayment: <Number>, // номер платежа в заголовке (Платёж между счетами № ...)
  idCompany: company.id, // идентификатор компании, из которой осуществляется платеж
  status: 'noSignature', // статус платежа (отправлен, в процессе, не подписан)
  createdAt: <Date>, // дата создания платежа
  type: 'inCustom', // тип платежа - в таможню
  purpose: <String>, // назначение
  sum: <Number>, // сумма на выбранном счете
  accountNumber: <String>, // номер счета
  account: <Number>, // идентификатор счета, с которого осуществляется платеж
  recipient: { // получатель
      title: <String>, // наименование организации/ФИО
      number: <String>, // ИНН
      code: <String>, // КПП
  },
  bank: <Object> {
              title: <String>, // название банка
              bic: <String>, // БИК
              correspondentAcc: <String>, // корреспондентский счет
          },
  kbk: <String>, // КБК
  uin: <String>, // уникальный идентификатор начисления
  payer: { // плательщик
      title: <String>, // наименование организации/ФИО
      number: <String>, // ИНН
  },
  classifier: <String>, // ОКТМО
  basisPayment: <String>, // Основание платежа
  taxablePeriod: <String>, // Налоговый период
  numberDocument: <String>, // В требовании от налоговой
  dateDocument: <String>,// Дата документа (поле для выбора даты в календаре)
  codeStatus: <String>,// Код статуса плательщика
  codeCustom: <String>, // Код таможенного органа
}
```

### 3.5. Получение списка организаций/банков для автозаполнения.

request: GET /search/:word/:list

[response](./stubbs/Payments/autocomplete.json)

```text
{
  "listRecipients": [ // список получателей
    {
      "title": "ООО «Рабочая группа людей»", // наименование организации
      "number": "0645146088", // ИНН
      "code": "301018105" // КПП
    },
    ...
  ],
  "listBanks": [ // список банков
    {
      "title": "АБАКАНСКОЕ ОТДЕЛЕНИЕ №8602 ПАО СБЕРБАНК", // наименование банка
      "bic": "049514608", // БИК
      "correspondentAcc": "30101810500000000608" // корреспондентский счет
    },
    ...
  ]
}
```   

### 3.6. Платежи "В бюджет" и "В таможню".
#### 3.6.1. получение списка кодов статусов плательщиков:

request: GET /payers

[response](./stubbs/Payments/payers.json)
```text
[
  {
    "value": "09", // уникальное значение
    "name": "09 - налогоплательщик (плательщик сборов) - индивид..." // значение, которое отображается на фронте в select
  },
  ...
]

```

#### 3.6.2. получение списка оснований платежа:

request: GET /basis

[response](./stubbs/Payments/basis.json)
```text
[
    {
        "value": "AP", // уникальное значение
        "name": "АП - погашение задолженности по акту проверки" // значение, которое отображается на фронте в select
    }
]

```

#### 3.6.3. получение списка налоговых периодов:

request: GET /taxPeriods

[response](./stubbs/Payments/taxPeriods.json)
```text
[
    {
        "value": "QT", // уникальное значение
        "name": "КВ - квартальный платёж" // значение, которое отображается на фронте в select
    }
]
```

### 3.7 Платежи Из файла.
#### 3.7.1 Отправка файла на сервер

request: POST /payments/from_file

payload: <Filedata>

response: `{ anyField: true } ` или статус 200 о том что все ок,
показывает что файл удачно залит на сервер

#### 3.7.2. Получение данных из загруженного файла

request: GET /payments/from_file

response: 

```text
{
  bad: [
    {
      errors: ["Ошибка в реквизитах счёта для списания",…] // массив ошибок при распознавании платежа
      id: 633408 // идентификатор платежа
      nameCompany: "ИП Стурза Наталья Александровна" // имя компании, которая платит
      sum: 16860 // сумма указанная в платеже
    }
    ,…
    ]
  good: [ // объект аналогичный объекту выше, но без массива ошибок
    {
    id: 633408, 
    sum: 16860, 
    nameCompany: "ИП Стурза Наталья Александровна"
    }
    ,…
  ]
} объект с удачно распознанными платежами и распознанными с ошибками
```


#### 3.7.3 Отправка распознанных платежей на подпись

request: POST /payments/to_sign

payload: 
```text
[
 {
 id: 633408, 
 sum: 16860, 
 nameCompany: "ИП Стурза Наталья Александровна"
 }
]
```

#### 3.7.4 Удаление распознанных платежей 

request: DELETE /payments/to_delete

payload:  
```text
[
 {
 id: 633408, 
 sum: 16860, 
 nameCompany: "ИП Стурза Наталья Александровна"
 }
]
```


## 4. Настройки
### 4.1. Вход в банк  
Для этой страницы используются запросы:
#### 4.1.1. При вводе старого пароля делается запрос через секунду после окончания ввода.

request POST /settings/check_old_password

payload: `{ password: <String> "Old password"}`

response: `{ anyField: true } ` или статус 200 о том что все ок, введенный пароль соответствует паролю на сервере

#### 4.1.2. По нажатию кнопки сменить пароль.

request POST Request URL: /settings/save_new_password

payload: `{ newPassword: <String> "New Password" }`

response: `{ anyField: true } ` или статус 200 о том что все ок

### 4.2. Уведомления  
Для это страницы используются запросы:
#### 4.2.1. Получение всех контактов для текущего пользователя.

request GET /settings/notifications

 [response](./stubbs/Settings/Notifications/index.json)
```text
{
  "phone": <String> "+7 (123) 131-23-12", // основной номер пользователя
  "phonesOperations": <ArrayOf(<Object>)>[ // секция операции по счёту
    {
      "id": <String> 0, // уникальный идентификатор контакта
      "data": <String> "+7 (123) 131-23-12", // номер телефона
      "saved": <Boolean> true // для подтверждения что сохранено на сервер
    }
  ],
  "emailsOperations": [ // секция операции по счёту
    { 
      "id": 1548235590181,
      "data": "wqeqwe@qe.com",
      "saved": true
    },
  ],
  "emailsForNews": [
   { 
    "id": 1548235590181,
    "data": "wqeqwe@qe.com",
    "saved": true
  },
  ] //рассылка новостей
}
```

#### 4.2.2. Смена основного номера.

request PUT /settings/notifications/main_phone

payload: `{ newPhone: <String> +7 (123) 131-23-15 }`

response: `{ anyField: true } ` или статус 200 о том что все ок
   
#### 4.2.3. Обновление контактов для операций по счёту и рассылки новостей.

request PUT /settings/notifications/contacts

payload:
```text
contact: {
  name: "phonesOperations" // имя массива контактов, которое нужно обновить
  newData: {
      id: 1551873183430, 
      data: "+7 (342) 342-34-23", 
      saved: false 
  }
```
response: `{ anyField: true } ` или статус 200 о том что все ок

## 5. Тарифы  
Для этой страницы используются запросы:

### 5.1. Получение всех тарифов

request: GET /settings/tarrifs

[response](./stubbs/Settings/Tarrifs/index.json)

```text
[{
    name: <String>,
    monthlyPay: <Number>, //Ежемесячная абонентская плата
    description: <String>,
    freePay: <Number> //Бесплатных платежей
    paysRuble: <Number>, //Платежей в рублях
    nextPays: <Number> //Все последующие платежи
    currency: <String>,
    payToCurrency: <String>,
    discountAgent: <String>, //Скидка на услуги агента ВК
    openCurrencyAccount: <String>, //Открытие валютных счетов
    currencyControl: <String>, 
    transferLegal: <ArrayOf[<String>]>, //Переводы на счета юрлиц
    transferPhysical: <ArrayOf[<String>]>, //Переводы на счета физлиц 
    entryCash: <ArrayOf[<String>]>, //Внесение наличных
    withdrawCash: <ArrayOf[<String>]>, //Выдача наличных
    transfersAndPayments: <Object> {
        transfers: <ArrayOf[<String>]>, //Переводы на счета физлиц
        payments: <ArrayOf[<String>]>,  //Переводы и платежи 
    }
}]
```

### 5.2. Переход на новый тариф

request: POST /settings/tarrifs

payload: `{ newTarrif: <Number> 0 }` id нового тарифа

response: `{ anyField: true } ` или статус 200 о том что все ок

возможно нужно передавать так же id текущей компании, не понятно из макетов 
за кем закреплены тарифы.

## 6. Карточка компании  
Для этой страницы используются запросы:

### 6.1. Получение информации о компании.

request: GET /companies/:id

[response](./stubbs/CardOfCompany/cards.json)

```text
{
   fullname: <Object> {
       type: <String>,
       name: <String>,
   }, //Полное наименование
   shortName: <String>, 
   number: <String>, //ИНН
   regNumberIE: <String>, // ОГРНИП
   code: <String>, //КПП
   regNumber: <String>, //ОГРН
   legalAddress: <String>,
   address: <String>,
   phone: <String>,
   email: <String>,
   bank: <Object> {
       name: <String>
       bic: <String>
       correspodentAcc: <String>
       checkingAcc: <String>
   },
   id: <String>,
   CEO: <String>,
   checkingAccounts: <ArrayOf[account: <Object>]> // счета текущей компании, они должны быть такими же как для сайдер меню п.1.1
    {
        "title": <String>,
        "sum": <Number>,
        "number": <String> // номер счета
    }
}
```

### 6.2. Изменение данных в карточке (адрес, телефон или email)

request: PUT /companies/:id

payload: 
```text
{
  fullname: <Object> {
      type: <String>,
      name: <String>,
  }, //Полное наименование
  shortName: <String>, 
  number: <String>, //ИНН
  regNumberIE: <String>, // ОГРНИП
  code: <String>, //КПП
  regNumber: <String>, //ОГРН
  legalAddress: <String>, // юридический адрес компании
  address: <String>, // фактический адрес
  phone: <String>, 
  email: <String>,
  bank: <Object> {
      name: <String> // имя банка
      bic: <String> // БИК
      correspodentAcc: <String> // корр. счёт
      checkingAcc: <String> // расчётный счёт
  },
  id: <String>,
  CEO: <String>,
  checkingAccounts: <ArrayOf[account: <Object>]> // счета текущей компании
   {
       "title": <String>,
       "sum": <Number>,
       "number": <String> // номер счета
   }
}карточка компании с измененным полем
```
          
response: `{ anyField: true } ` или статус 200 о том что все ок

### 6.3. Отправка карточки на указанные emails

request: POST companies/:id/current_company_to_emails

payload: 
```text
{
doc: {
   fullname: <Object> { // аналогично 6.2
       type: <String>,
       name: <String>,
   }, //Полное наименование
   shortName: <String>, 
   number: <String>, //ИНН
   regNumberIE: <String>, // ОГРНИП
   code: <String>, //КПП
   regNumber: <String>, //ОГРН
   legalAddress: <String>,
   address: <String>,
   phone: <String>,
   email: <String>,
   bank: <Object> {
       name: <String>
       bic: <String>
       correspodentAcc: <String>
       checkingAcc: <String>
   },
   id: <String>,
   CEO: <String>,
   checkingAccounts: <ArrayOf[account: <Object>]> // счета текущей компании {
        "title": <String>,
        "sum": <Number>,
        "number": <String> // номер счета
    }
 },
 emails: [
    id: 1551873183430, 
    data: "email@qwe.com", 
    saved: false  // внутренняя переменная, в данном случае не обязательно для сервера, в других ситуациях показывает сохранен ли email на сервере
  ]
}
```
  
response: `{ anyField: true } ` или статус 200 о том что все ок

## 7. Выставление счета  
Для данной страницы используются запросы:

### 7.1. Получение последних счетов.
request GET /invoices/bill

[response](./stubbs/Invoices/bills.json)
```text
[{
    idPayment: <String>,
    numberBill/numberAct: <Number>, // номер документа как для счёта так и для акта
    recipient: <Object> { // получатель счёта, аналогично п. 3.4
        title: <String>, 
        number: <String>, 
        code: <String>,
    },
    bank: <Object> { // получатель счёта, аналогично п. 3.4
        title: <String>,
        bic: <String>,
        correspodentAcc: <String>
    },
    numberTax: <String>, // ИНН
    code: <String> // опционально КПП
    accountNumber: <String>, // номер счёта банка
    purpose: <String> // условия оплаты
    calendarDate: <Date|String>, // "2011-03-15T21:00:00.000Z" 
    tickets: <ArrayOf[<Object> { // массив платежек поля За Что
        id: <Number>, 
        amount: <Number>,  // количество продукта
        price: <Number>, // цена продукта
        product: <String> // имя продукта
    }]>,
    date: <String> // имя платежа с датой сформированное на стороне фронта, отображается в списке предыдущих счетов
    type: <String>, // счёт или акт bill/act
    sum: <Number>, // общая сумма, высчитывается на основе tickets
    legalAddress: <String>,
    nds: <Number>, // размер ндс
    taxes: <String> // "НДС 20%"
}]
```

### 7.2. Редактирование платежа после нажатия на элемент из списка последних или нажатие кнопки редактирование на странице созданного счёта.
    
request GET /invoices/bill/:id

[response](./stubbs/Invoices/bills.json)
[п. 7.1](#7.1.-получение-последних-счетов.)
но должны получить только один счёт.

### 7.3. Создание или сохранение счета.

request: POST /invoices

payload: 
[п. 7.1](#bill) один объект у которого добавляются поля представленные снизу
```text
{
  stamp: <String>, // картинка с печатью, возможно они загружаться должны отдельно на s3 или еще куда, а не слаться вместе с формой
  signature: <String>, // картинка с подпись, возможно они загружаться должны отдельно на s3 или еще куда, а не слаться вместе с формой
}
```
         
[response](./stubbs/Invoices/pdf.json)
массив ссылок или ссылка на сформированный документ. 
`docUrl: <ArrayOf[<String>]>`
 
### 7.4. Повторение счета
используется запрос 7.2, на клиенте url выглядит как /invoices/bill?replay=id
где id это идентификатор счёта, данные из которого нужно взять для нового счёта
    
### 7.5. Создание акта по счету
используется запрос 7.2, на клиенте url выглядит как /invoices/act?basedOn=id
где id это идентификатор счёта, данные из которого нужно взять для нового акта

### 7.6. Отправка сформированного счёта на emails.

request: POST /invoices/send_to_emails

payload:
[п. 7.1](#bill) один объект, либо сами файлы полученные после формирования счёта.
```text
{
 { п. 7.1 }
   },
   emails: [
     id: 1551873183430, // внутренняя переменная не важная для сервера 
     data: "emails@qwe.ru", 
     saved: true // внутренняя переменная не важная для сервера
  ]
}
```
response: `{ anyField: true } ` или статус 200 о том что все ок
    
## 8. Формирование акта  
Для данной страницы используются запросы:

### [8.1. Получение последних актов.](#act)

request GET /invoices/act

[response](./stubbs/Invoices/acts.json)
```text
[{
 {
   idPayment: <String>,
   numberBill/numberAct: <Number>, // номер документа как для счёта так и для акта
   recipient: <Object> { // получатель счёта, аналогично п. 3.4
       title: <String>, 
       number: <String>, 
       code: <String>,
   },
   bank: <Object> { // получатель счёта, аналогично п. 3.4
       title: <String>,
       bic: <String>,
       correspodentAcc: <String>
   },
   numberTax: <String>, // ИНН
   code: <String> // опционально КПП
   accountNumber: <String>, // номер счёта банка
   calendarDate: <Date|String>, // "2011-03-15T21:00:00.000Z" 
   tickets: <ArrayOf[<Object> { // массив платежек поля За Что
       id: <Number>, 
       amount: <Number>,  // количество продукта
       price: <Number>, // цена продукта
       product: <String> // имя продукта
   }]>,
   date: <String> // имя платежа с датой сформированное на стороне фронта, отображается в списке предыдущих счетов
   type: <String>, // счёт или акт bill/act
   sum: <Number>, // общая сумма, высчитывается на основе tickets
   legalAddress: <String>,
   nds: <Number>, // размер ндс
   taxes: <String> // "НДС 20%"
 }
 basedOnBill: <Boolean> // значение чекбокса на основе счёта 
}]
```

### 8.2. Редактирование акта  нажатием на элемент из списка последних или нажатие кнопки редактирование на странице созданного акта.

request GET /invoices/act/:id

[response](./stubbs/Invoices/acts.json):

[п. 8.1](#act)
 
но должны получить только один акт.

### 8.3. Создание или сохранение акта.

request: POST /invoices

payload: 
  [п. 8.1](#act)  объект которого добавляются поля представленные снизу
```text
{
  stamp: <String>, // картинка с печатью, возможно они загружаться должны отдельно на s3 или еще куда, а не слаться вместе с формой
  signature: <String>, // картинка с подпись, возможно они загружаться должны отдельно на s3 или еще куда, а не слаться вместе с формой
  }
 ```
         
[response](./stubbs/Invoices/pdf.json)
массив ссылок или ссылка на сформированный документ. 
`docUrl: <ArrayOf[<String>]>`
 
### 8.4. Повторение акта
используется запрос 8.2, на клиенте url выглядит как /invoices/act?replay=id
где id это идентификатор счёта, данные из которого нужно взять для нового счёта
    
### 8.5. Создание счёта по акту
используется запрос 8.2, на клиенте url выглядит как /invoices/bill?basedOn=id
где id это идентификатор акта, данные из которого нужно взять для нового акта
    
### 8.6. Отправка сформированного акта на emails.

request: POST /invoices/send_to_emails

payload: 
  [п. 8.1](#act) один объект, либо сами файлы полученные после формирования счёта.
  и поле email - массив email
```text
{
   { п. 8.1 }, 
  emails: [
    id: 1551873183430, // внутренняя переменная не важная для сервера 
    data: "emails@qwe.com", 
    saved: true // внутренняя переменная не важная для сервера
  ]
}
```
response: `{ anyField: true } ` или статус 200 о том что все ок

## 9. Выписка  
Для данной страницы используются запросы:
  
### 9.1. Получение регулярных выписок.

request: GET /statements

[response](./stubbs/Statements/index.json)
```text
[<ArrayOf<Object>>{
   account: <Number>,   // value для select из списка счетов
   accountNumber: <String>, // номер счета
   endDate: <String> //для единоразовой выписки 
   format: enum('PDF', '1C', 'Excel', 'CSV'),
   idPayment: <String>,
   period: <String>, // сообщение о периоде и формате для последних
   periodValue: <Number>, // для select с выбором периодов
   regularly: <Boolean>,
   startDate: <String>, //для единоразовой выписки  
   withPayments: <Boolean>, // значение checkbox "с платежами"
   emails: <ArrayOf[<String>]>, // массив email, куда будут рассыылаться выписки
}]
```  

На наш взгляд, запрос нужно перенести на этот URL:

request: GET /companies/:id/statements

###9.2. Получение конкретной выписки.
 
request: GET /statements/:id

[response](./stubbs/Statements/index.json)

```text
{
  account: <Number>, // value для select из списка счетов
  accountNumber: <String>, // номер счета
  endDate: <String> //для единоразовой выписки 
  format: enum('PDF', '1C', 'Excel', 'CSV'),
  idPayment: <String>,
  period: <String>, // сообщение о периоде и формате для последних
  periodValue: <Number>, // для select с выбором периодов
  regularly: <Boolean>,
  startDate: <String>, //для единоразовой выписки  
  withPayments: <Boolean>, // значение checkbox "с платежами"
  emails: [
    id: 1551873183430, // внутренняя переменная не важная для сервера 
    data: "emails@qwe.com", 
    saved: true
  ], // массив email, куда будут рассыылаться выписки
} 
```

На фронте выбирается из полученного массива берется только конкретная, с бэка должен возвращаться
объект представленного вида.  

На наш взгляд, запрос нужно перенести на этот URL:

request: GET /companies/:id/statements/:id

###9.3. Сохранение или создание выписки.

request: POST /statements

body:
```text
  {
    account: <Number>,
    accountNumber: <String>,
    endDate: <String> //для единоразовой выписки 
    format: enum('PDF', '1C', 'Excel', 'CSV'),
    idPayment: <String>,
    period: <String>, // сообщение о периоде и формате для последних
    periodValue: <Number>, // для select с выбором периодов
    regularly: <Boolean>,
    startDate: <String>, //для единоразовой выписки  
    withPayments: <Boolean>,
    emails: [
    id: 1551873183430, // внутренняя переменная не важная для сервера 
    data: "emails@qwe.com", 
    saved: true 
  ],
  }
```

response: `{ anyField: true } ` или статус 200 о том что все ок

На наш взгляд, запрос нужно перенести на этот URL:
        
request: POST /companies/:id/statements
    
###9.4. Удаление сохраненной выписки

request: DELETE /statements/:id
        
response: `{ anyField: true } ` или статус 200 о том что все ок

На наш взгляд, запрос нужно перенести на этот URL:
                
request: DELETE /companies/:id/statements/:id
                
##10. Восстановление пароля

### 10.1 Отправка логина для проверки его наличия в базе.
    
request: POST /password_restore
    
Тестовый логин: **test123**
    
Если логин правильный, то возвращается почта, на которую придет временная ссылка, и количество часов, сколько она будет действительна:
    
```
    {
      "emailForPass": "kari***9@gmail.com",
      "hours": 5
    }

```
    
### 10.2 Отправка нового пароля 
    
(после перехода по ссылке в почте, по сформированному адресу `auth/password-restore?emailLink=123`)

request: PUT /new_password

Новый пароль должен соответствовать требованиям: ` Добавьте минимум 1 цифру, заглавную букву и символ.  Исключите сочетания как 123 или qwe: их легко подобрать. `